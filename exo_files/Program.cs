﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo_files
{
    class Program
    {
        static void Main(string[] args)
        {
            if (!File.Exists(@"users.txt"))
            {
                //Console.WriteLine("Pas encore d'utilisateur");
                //File.Create(@"utilisateurs.txt");
                FileStream f = File.Create("users.txt");
                f.Close();
            }

            string[] utilisateurs = File.ReadAllLines("users.txt");
            string nom = "";
            List<string> users = new List<string>();
            foreach(string v in utilisateurs)
            {
                users.Add(v);
            }

            do
            {
                Console.Clear();
                foreach(string v in users)
                {
                    Console.WriteLine(v);
                }
                Console.WriteLine("Entrez les noms d'utilisateurs (STOP pour arrêter)");
                nom = Console.ReadLine();
                if(nom != "STOP")
                users.Add(nom);
            } while (nom != "STOP");

            File.WriteAllLines("users.txt", users);
        }
    }
}
