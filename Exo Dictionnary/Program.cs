﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo_Dictionnary
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, int> stock = new Dictionary<string, int>();
            int quantite;
            char action = '\0';
            string biere = null;
            bool quantiteValide;
            bool actionValide;

            //Console.WriteLine("Entrez une bière à sélectionner (STOP pour arrêter) : ");
            //biere = Console.ReadLine();

            while (true)
            {
                Console.Clear();
                Console.WriteLine("Entrez une bière à sélectionner : ");
                biere = Console.ReadLine().ToUpper();
                if (!(stock.ContainsKey(biere)))
                {
                    stock.Add(biere, 0);
                }
                quantite = stock[biere];
                Console.WriteLine($"Bière : {biere}.");
                Console.WriteLine($"Stock : {quantite}.");
                if ((quantite != 0))
                {
                    do
                    {
                        Console.WriteLine("Souhaitez-vous en ajouter(+), en enlever(-) ou modifier les stocks(*)?");
                        actionValide = (
                                        (char.TryParse(Console.ReadLine(), out action)) &&
                                        ((action == '+') || (action == '-') || (action == '*'))
                                       );
                        if (!actionValide)
                        {
                            Console.WriteLine($"Erreur : Entrez une action valide svp! (+ pour ajouter, - pour enlever, * pour modifier le stock.");
                        }
                    } while (!actionValide); 
                }
                if((action == '+') || (quantite == 0))
                {
                    do
                    {
                        Console.WriteLine("Quelle quantité souhaitez-vous ajouter?");
                        quantiteValide = (int.TryParse(Console.ReadLine(), out quantite)) && (quantite >= 0);
                        if (!quantiteValide)
                        {
                            Console.WriteLine($"Erreur : Entrez une quantité valide svp !");
                        }
                    } while (!quantiteValide);
                    stock[biere] += quantite;
                }
                else if (action == '-')
                {
                    do
                    {
                        Console.WriteLine("Quelle quantité souhaitez-vous retirer?");
                        quantiteValide = (int.TryParse(Console.ReadLine(), out quantite)) && (quantite >= 0);
                        if (!quantiteValide)
                        {
                            Console.WriteLine($"Erreur : Entrez une quantité valide svp !");
                        }
                        if (quantite > stock[biere])
                        {
                            Console.WriteLine("Erreur : vous essayez de retirer plus de bières qu'il n'y en a en stock!");
                        }
                    } while (!quantiteValide ||(quantite > stock[biere]));
                    stock[biere] -= quantite;
                }
                else
                {
                    do
                    {
                        Console.WriteLine("Entrez la quantité à ajuster : ");
                        quantiteValide = (int.TryParse(Console.ReadLine(), out quantite)) && (quantite >= 0);
                        if (!quantiteValide)
                        {
                            Console.WriteLine($"Erreur : Entrez une quantité valide svp !");
                        }
                    } while (!quantiteValide);
                    stock[biere] = quantite;
                }
                if(stock[biere] == 0)
                {
                    stock.Remove(biere);
                }
                Console.Clear();
                if(stock.Count != 0)
                {
                    Console.WriteLine("Etat du stock : ");
                    foreach (KeyValuePair<string, int> kvp in stock)
                    {
                        Console.WriteLine($"Bière : {kvp.Key}, Stock : {kvp.Value}");
                    }
                }
                else
                {
                    Console.WriteLine("Le stock est vide!");
                }
                Console.WriteLine("\nAppuyez sur une touche pour continuer...");
                Console.ReadKey();
            }
        }
    }
}